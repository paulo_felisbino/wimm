require 'rails_helper'

RSpec.describe DashboardChannel do
  before do
    company = create_company
    user = create_user(company)
    stub_connection current_user: user
    @dashboard_stream_name = "dashboard_#{user.id}"
    @dashboard_components_stream_names = [
      "notification_#{user.id}",
      "number_of_plans_#{user.id}",
      "number_of_hired_hours_#{user.id}",
      "number_of_consumed_hours_#{user.id}",
      "number_of_remaining_hours_#{user.id}",
      "charts_#{user.id}",
      "loading_#{user.id}"
    ]
    subscribe
  end

  it 'subscribes to dashboard stream' do
    expect(subscription).to be_confirmed
    expect(streams).to include(@dashboard_stream_name)
    expect(streams).to include(@dashboard_components_stream_names[0])
    expect(streams).to include(@dashboard_components_stream_names[1])
    expect(streams).to include(@dashboard_components_stream_names[2])
    expect(streams).to include(@dashboard_components_stream_names[3])
    expect(streams).to include(@dashboard_components_stream_names[4])
    expect(streams).to include(@dashboard_components_stream_names[5])
    expect(streams).to include(@dashboard_components_stream_names[6])
  end

  it 'broadcasts to dashboard stream' do
    broadcast_object = { data: 'Hello, Rails!' }
    assert_broadcasts @dashboard_stream_name, 0
    ActionCable.server.broadcast @dashboard_stream_name, broadcast_object
    assert_broadcasts @dashboard_stream_name, 1
  end

  context 'rebroadcasts to all dashboard components' do
    before(:each) do
      @broadcast_object = { data: 'Hello, Rails!' }
    end

    it 'rebroadcasts to dashboard notification channel' do
      stream = @dashboard_components_stream_names[0]
      assert_broadcasts(stream, 1) do
        perform :send_to_dashboard_components, @broadcast_object
      end
    end

    it 'rebroadcasts to dashboard number_of_plans channel' do
      stream = @dashboard_components_stream_names[1]
      assert_broadcasts(stream, 1) do
        perform :send_to_dashboard_components, @broadcast_object
      end
    end

    it 'rebroadcasts to dashboard number_of_hired_hours channel' do
      stream = @dashboard_components_stream_names[2]
      assert_broadcasts(stream, 1) do
        perform :send_to_dashboard_components, @broadcast_object
      end
    end

    it 'rebroadcasts to dashboard number_of_consumed_hours channel' do
      stream = @dashboard_components_stream_names[3]
      assert_broadcasts(stream, 1) do
        perform :send_to_dashboard_components, @broadcast_object
      end
    end

    it 'rebroadcasts to dashboard number_of_remaining_hours channel' do
      stream = @dashboard_components_stream_names[4]
      assert_broadcasts(stream, 1) do
        perform :send_to_dashboard_components, @broadcast_object
      end
    end

    it 'rebroadcasts to dashboard charts channel' do
      stream = @dashboard_components_stream_names[5]
      assert_broadcasts(stream, 1) do
        perform :send_to_dashboard_components, @broadcast_object
      end
    end

    it 'rebroadcasts to dashboard loading channel' do
      stream = @dashboard_components_stream_names[6]
      assert_broadcasts(stream, 2) do
        perform :send_to_dashboard_components, @broadcast_object
      end
    end
  end
end
