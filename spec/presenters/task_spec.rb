require 'rails_helper'

RSpec.describe TaskPresenter do
  before(:each) do
    @today = DateTime.now
    @company = FactoryGirl.create(:company)
    @plan = FactoryGirl.create(:plan)
    period = FactoryGirl.create(:period, month: @today.month, year: @today.year)
    contract = FactoryGirl.create(:contract, company: @company, plan: @plan, period: period)
    @task = FactoryGirl.create(:task, task_date: @today, contract: contract)

    @task_presenter = TaskPresenter.new([@task], nil)
  end

  describe '#initialize' do
    it 'set variables to be used on presenter' do
      expect(@task_presenter.tasks.include?(@task)).to be true
    end
  end

  describe '#task_plan_name' do
    context 'given a task' do
      it 'returns the plan name' do
        expected_response = @plan.name
        expect(@task_presenter.task_plan_name(@task)).to eq(expected_response)
      end
    end
  end

  describe '#task_date_to_s' do
    context 'given a task' do
      it 'returns the task date formated as string' do
        expected_response = @today.strftime('%d/%m/%Y')
        expect(@task_presenter.task_date_to_s(@task)).to eq(expected_response)
      end
    end
  end
end
