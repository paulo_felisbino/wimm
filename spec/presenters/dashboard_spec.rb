require 'rails_helper'

RSpec.describe DashboardPresenter do
  def human_readable_decimal_as_hour(big_decimal)
    DecimalToTimeFormatter.convert_big_decimal_to_hour_format(big_decimal)
  end

  before(:each) do
    @today = DateTime.now
    @company = FactoryGirl.create(:company)
    @user = FactoryGirl.create(:user, company: @company)
    @plan = FactoryGirl.create(:plan)
    @period = FactoryGirl.create(:period, month: @today.month, year: @today.year)
    @contract = FactoryGirl.create(:contract, company: @company, plan: @plan, period: @period, hours_hired: 20)

    @date_info = { month: @today.month, year: @today.year }
    contracts = [@contract]
    @dashboard = DashboardPresenter.new(contracts, @date_info, contracts, @contract.id)
  end

  describe '#initialize' do
    it 'set variables to be used on presenter' do
      expect(@dashboard.current_date_info).to eq(@date_info)
      expect(@dashboard.contracts.include?(@contract)).to be true
      expect(@dashboard.selected_contracts.include?(@contract)).to be true
      expect(@dashboard.selected_contract).to eq(@contract.id)
    end
  end

  describe '#number_plans' do
    it 'returns the quantity of contracts for current company' do
      expect(@dashboard.number_plans).to eq(1)
    end
  end

  describe '#number_hired_hours' do
    it 'returns the quantity of hired hours for all company contracts' do
      expected_hired_hours = human_readable_decimal_as_hour(@contract.hours_hired)
      expect(@dashboard.number_hired_hours).to eq(expected_hired_hours)
    end
  end

  describe '#number_consumed_hours' do
    it 'returns the quantity of consumed hours for all company contracts' do
      task = FactoryGirl.create(:task, task_date: @today, contract: @contract, duration: 5)
      expected_consumed_hours = human_readable_decimal_as_hour(task.duration)
      expect(@dashboard.number_consumed_hours).to eq(expected_consumed_hours)
    end
  end

  describe '#number_remaining_hours' do
    it 'returns the quantity of remaining hours for all company contracts' do
      task = FactoryGirl.create(:task, task_date: @today, contract: @contract, duration: 5)
      expected_remaining_hours = human_readable_decimal_as_hour(@contract.hours_hired - task.duration)
      expect(@dashboard.number_remaining_hours).to eq(expected_remaining_hours)
    end
  end

  describe '#dashboard_year' do
    it 'returns the current year for the presenter' do
      expect(@dashboard.dashboard_year).to eq(@today.year)
    end
  end

  describe '#remaining_hours_message_partial_name' do
    context 'returns the name of a partial depending on the quantity of remaining hours' do
      it 'returns "positive" if quantity of remaining hours >= 0' do
        FactoryGirl.create(:task, task_date: @today, contract: @contract, duration: 5)
        expect(@dashboard.remaining_hours_message_partial_name).to eq('positive')
      end
      it 'returns "negative" if quantity of remaining hours < 0' do
        FactoryGirl.create(:task, task_date: @today, contract: @contract, duration: 25)
        expect(@dashboard.remaining_hours_message_partial_name).to eq('negative')
      end
    end
  end
end
