require 'rails_helper'

RSpec.describe ScopePresenter do
  before(:each) do
    scopes = {
      all_scopes: %i[current_month all],
      default: :current_month
    }

    @current_scope = scopes[:default]
    @possible_scopes = scopes[:all_scopes]
    @scopes_paths = {
      all: {
        path: 'foo/bar',
        text: 'Foo'
      },
      current_month: {
        path: 'foo/bar',
        text: 'Bar'
      }
    }

    @scope_presenter = ScopePresenter.new(@current_scope, @possible_scopes, @scopes_paths)
  end

  describe '#initialize' do
    it 'set variables to be used on presenter' do
      expect(@scope_presenter.current_scope).to eq(@current_scope)
      expect(@scope_presenter.possible_scopes).to eq(@possible_scopes)
      expect(@scope_presenter.scopes_paths).to eq(@scopes_paths)
    end
  end

  describe '#use_scopes_paths' do
    context 'given a custom scopes paths' do
      it 'updates the presenter attribute scopes_paths' do
        custom_scopes_paths = {
          all: {
            path: 'other/foo/bar',
            text: 'Foo X'
          },
          current_month: {
            path: 'foo/bar',
            text: 'Bar'
          }
        }
        @scope_presenter.use_scopes_paths(custom_scopes_paths)

        expect(@scope_presenter.scopes_paths).to eq(custom_scopes_paths)
        expect(@scope_presenter.scopes_paths).not_to eq(@scopes_paths)
      end
    end
  end

  describe '#scope_correct_class' do
    describe 'returns the appropriate class for a passed scope' do
      context 'given a scope that matches the current scope' do
        it 'returns "active-scope"' do
          scope_class = @scope_presenter.scope_correct_class(:current_month, @current_scope)
          expected_class = 'active-scope'
          expect(scope_class).to eq(expected_class)
        end
      end

      context 'given a scope that is differente from the current scope' do
        it 'returns an empty string' do
          scope_class = @scope_presenter.scope_correct_class(:all, @current_scope)
          expected_class = ''
          expect(scope_class).to eq(expected_class)
        end
      end
    end
  end
end
