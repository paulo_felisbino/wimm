require 'rails_helper'

RSpec.describe ContractPresenter do
  before(:each) do
    today = DateTime.now
    company = FactoryGirl.create(:company)
    plan = FactoryGirl.create(:plan)
    @period = FactoryGirl.create(:period, month: today.month, year: today.year)
    @contract = FactoryGirl.create(:contract, company: company, plan: plan, period: @period)

    @contract_presenter = ContractPresenter.new([@contract], nil)
  end

  describe '#initialize' do
    it 'set variables to be used on presenter' do
      expect(@contract_presenter.contracts.include?(@contract)).to be true
    end
  end

  describe '#plan_name' do
    context 'given a plan associated to a contract' do
      it 'returns the plan name' do
        today = DateTime.now
        company = FactoryGirl.create(:company)
        plan = FactoryGirl.create(:plan)
        period = FactoryGirl.create(:period, month: today.month, year: today.year)
        contract = FactoryGirl.create(:contract, company: company, plan: plan, period: period)

        expected_response = contract.plan.name
        response = @contract_presenter.plan_name(contract.plan)
        expect(response).to eq(expected_response)
      end
    end
  end

  describe '#reference_date_to_s' do
    context 'given a contract' do
      it 'returns a string with a contract period as "month/year"' do
        expected_string = "#{@period.month.to_s.rjust(2, '0')}/#{@period.year}"
        expect(@contract_presenter.reference_date_to_s(@contract.period)).to eq(expected_string)
      end
    end
  end
end
