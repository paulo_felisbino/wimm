require 'rails_helper'

RSpec.describe ReportsHelper do
  describe '#human_readable_time' do
    it 'returns string with format "January/2018" given month number and year with format "1_2018"' do
      expected_response = 'January/2018'
      response = helper.human_readable_time('1_2018')
      expect(response).to eq(expected_response)
    end
  end

  describe '#contract_plan_name' do
    context 'given a plan associated to a contract' do
      it 'returns the plan name' do
        today = DateTime.now
        company = FactoryGirl.create(:company)
        plan = FactoryGirl.create(:plan)
        period = FactoryGirl.create(:period, month: today.month, year: today.year)
        contract = FactoryGirl.create(:contract, company: company, plan: plan, period: period)

        expected_response = contract.plan.name
        response = helper.contract_plan_name(contract.plan)
        expect(response).to eq(expected_response)
      end
    end
  end
end
