require 'rails_helper'

RSpec.describe Contract do
  it 'has a valid factory' do
    contract = FactoryGirl.create(:contract)
    expect(contract.save).to be true
  end

  it 'is invalid without a company' do
    contract = FactoryGirl.build(:contract, company_id: nil)
    expect(contract.save).to be false
  end

  it 'is invalid without a plan' do
    contract = FactoryGirl.build(:contract, plan_id: nil)
    expect(contract.save).to be false
  end

  it 'is invalid without a period' do
    contract = FactoryGirl.build(:contract, period_id: nil)
    expect(contract.save).to be false
  end
end
