require 'rails_helper'

RSpec.describe Task do
  it 'has a valid factory' do
    contract = FactoryGirl.create(:contract)
    task = FactoryGirl.build(:task, contract: contract)
    expect(task.save).to be true
  end

  it 'is invalid without a description' do
    task = FactoryGirl.build(:task, description: nil)
    expect(task.save).to be false
  end

  it 'is invalid without a contract' do
    task = FactoryGirl.build(:task, contract_id: nil)
    expect(task.save).to be false
  end
end
