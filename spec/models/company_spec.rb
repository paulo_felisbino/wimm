require 'rails_helper'

RSpec.describe Company do
  it 'has a valid factory' do
    expect(FactoryGirl.build(:company).save).to be true
  end

  it 'is invalid without a name' do
    expect(FactoryGirl.build(:company, name: nil).save).to be false
  end
end
