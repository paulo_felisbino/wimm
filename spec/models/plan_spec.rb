require 'rails_helper'

RSpec.describe Plan do
  it 'has a valid factory' do
    plan = FactoryGirl.build(:plan)
    expect(plan.save).to be true
  end

  it 'is invalid without a name' do
    plan = FactoryGirl.build(:plan, name: nil)
    expect(plan.save).to be false
  end
end
