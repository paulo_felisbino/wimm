require 'rails_helper'

RSpec.describe User do
  it 'has a valid factory' do
    expect(FactoryGirl.build(:user).save).to be true
  end

  it 'is invalid without a name' do
    expect(FactoryGirl.build(:user, name: nil).save).to be false
  end

  it 'is invalid without an email' do
    expect(FactoryGirl.build(:user, email: nil).save).to be false
  end

  it 'is invalid without a password' do
    expect(FactoryGirl.build(:user, password: nil).save).to be false
  end

  it 'is invalid without a company' do
    expect(FactoryGirl.build(:user, company_id: nil).save).to be false
  end
end
