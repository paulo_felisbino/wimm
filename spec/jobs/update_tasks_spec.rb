require 'rails_helper'

RSpec.describe UpdateTasksJob do
  include ActiveJob::TestHelper

  subject(:job) { described_class.perform_later }

  it 'queues the job' do
    expect { job }.to have_enqueued_job(described_class)
      .on_queue('default')
  end

  it 'executes perform' do
    task_updater = instance_double(Updaters::TaskUpdater, update_tasks: '')
    allow(Updaters::TaskUpdater).to receive(:new).and_return(task_updater)

    perform_enqueued_jobs { job }

    expect(task_updater).to have_received(:update_tasks).at_least(1).times
  end

  after do
    clear_enqueued_jobs
    clear_performed_jobs
  end
end
