require 'rails_helper'

RSpec.describe 'user sign out' do
  let(:company) { create(:company) }
  let(:user) { create(:user, company: company) }

  before :each do
    visit '/'
    fill_in 'Email address', with: user.email
    fill_in 'Password', with: user.password
    click_on 'Sign in'
  end

  scenario 'successfully' do
    click_on 'Logout'
    expect(current_path).to eq('/users/sign_in')
  end
end
