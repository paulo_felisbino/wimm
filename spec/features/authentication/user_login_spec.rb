require 'rails_helper'

RSpec.describe 'user sign in' do
  let(:user) { create(:user) }

  scenario 'successfully' do
    visit '/'
    fill_in 'Email address', with: user.email
    fill_in 'Password', with: user.password
    click_on 'Sign in'

    expect(current_path).to eq('/')
  end
end
