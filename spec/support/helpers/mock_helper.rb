module Helpers
  module MockHelper
    def sign_in_user_and_mock_current_user_from_controller(user, controller)
      sign_in user
      mock_current_user_from_controller(controller, user)
    end

    def mock_current_user_from_controller(controller, user)
      allow(controller).to receive(:current_user).and_return(user)
    end
  end
end
