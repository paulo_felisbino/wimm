module Helpers
  module DatabaseHelper
    def create_company(name = nil)
      return FactoryGirl.create(:company, name: name) if name.present?
      FactoryGirl.create(:company)
    end

    def create_user(company = nil)
      return FactoryGirl.create(:user, company: company) if company.present?
      FactoryGirl.create(:user)
    end

    def create_plan(name = nil)
      return FactoryGirl.create(:plan, name: name) if name.present?
      FactoryGirl.create(:plan)
    end

    def create_period(date_param)
      date = date_param || DateTime.now
      FactoryGirl.create(:period, month: date.month, year: date.year)
    end

    # rubocop:disable Style/BracesAroundHashParameters
    def create_contract(company, plan, period, hours_hired = nil)
      return FactoryGirl.create(:contract, { company: company, plan: plan, period: period, hours_hired: hours_hired }) if hours_hired.present?
      FactoryGirl.create(:contract, company: company, plan: plan, period: period)
    end
    # rubocop:enable Style/BracesAroundHashParameters

    def create_task(task_date, contract, assigned_user_id = nil)
      return FactoryGirl.create(:task, task_date: task_date, contract: contract, assigned_user_id: assigned_user_id) if assigned_user_id.present?
      FactoryGirl.create(:task, task_date: task_date, contract: contract)
    end

    def create_task_and_contract_for_date(company, plan, date)
      contract = create_period_and_contract_for_date(company, plan, date)
      create_task(date, contract)
    end

    def create_period_and_contract_for_date(company, plan, date)
      period = create_period(date)
      create_contract(company, plan, period)
    end
  end
end
