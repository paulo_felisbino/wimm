module Helpers
  module ReportsHelper
    def create_base_objects_for_index
      company = create_company
      { company: company, user: create_user(company) }
    end

    def create_base_objects_for_create
      company = create_company
      plan = create_plan
      date = Date.today
      period = create_period(date)
      { company: company, user: create_user(company), plan: plan, date: date, period: period, contract: create_contract(company, plan, period) }
    end

    def create_base_objects_for_export_xls
      create_base_objects_for_create
    end

    def create_objects_for_post_and_expectations(date)
      start_date = date.beginning_of_month
      end_date = date.end_of_month
      date_group = "#{date.month}_#{date.year}"
      params = create_post_params_for_create(start_date, end_date)
      { params: params, start_date: start_date, end_date: end_date, date_group: date_group }
    end

    def create_objects_for_post_and_expectations_with_old_date(date)
      start_date = (date - 1.year).beginning_of_month
      end_date = date.end_of_month
      date_group = "#{date.month}_#{date.year}"
      params = create_post_params_for_create(start_date, end_date)
      { params: params, start_date: start_date, end_date: end_date, date_group: date_group }
    end

    def create_post_params_for_create(start_date, end_date)
      { format: 'js', objects_report_object: { start_date: start_date.to_s(:date_format), end_date: end_date.to_s(:date_format) } }
    end

    def create_objects_for_post_and_expectations_for_export(date)
      start_date = date.beginning_of_month
      end_date = date.end_of_month
      date_group = "#{date.month}_#{date.year}"
      params = create_post_params_for_export(start_date, end_date)
      { params: params, start_date: start_date, end_date: end_date, date_group: date_group }
    end

    def create_post_params_for_export(start_date, end_date)
      { objects_report_object: { start_date: start_date.to_s(:date_format), end_date: end_date.to_s(:date_format) } }
    end
  end
end
