module Helpers
  module TaskUpdaterHelper
    def create_base_objects
      objects_base = create_objects
      create_tasks_for_contract(objects_base[:date], objects_base[:contract], objects_base[:user].id)
      task_repository = Repositories::FakeTaskRepository.new
      { task_repository: task_repository, period: objects_base[:period] }
    end

    private

    def create_objects
      date = create_date_for_february
      period = create_period(date)
      user = create_user_with_company
      contract = create_contract_for_ticket(period)
      { date: date, period: period, user: user, contract: contract }
    end

    def create_date_for_february
      Date.today.change(month: 2).change(year: 2018)
    end

    def create_user_with_company
      company = create_company
      create_user(company)
    end

    def create_contract_for_ticket(period)
      ticket_objects = create_ticket_objects
      create_contract(ticket_objects[:company], ticket_objects[:plan], period, 69)
    end

    def create_ticket_objects
      company_ticket = create_company('Ticket')
      plan_ticket = create_plan('Ticket_Lead Machine')
      { company: company_ticket, plan: plan_ticket }
    end

    def create_tasks_for_contract(date, contract, user_id)
      create_task(date, contract, user_id)
      create_task(date, contract, user_id)
    end
  end
end
