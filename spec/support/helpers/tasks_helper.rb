module Helpers
  module TasksHelper
    def create_base_objects
      company = create_company
      { company: company, user: create_user(company), plan: create_plan, date: Date.today }
    end

    def create_task_for_current_date(company, plan, date)
      create_task_for_date(company, plan, date)
    end

    def create_task_for_old_date(company, plan, base_date)
      old_date = base_date - 1.year
      old_date -= 2.months
      create_task_for_date(company, plan, old_date)
    end

    def create_task_for_date(company, plan, date)
      create_task_and_contract_for_date(company, plan, date)
    end
  end
end
