module Helpers
  module ContractsHelper
    def create_base_objects
      company = create_company
      user = create_user(company)
      plan = create_plan
      { company: company, user: user, plan: plan, date: Date.today }
    end

    def create_contract_for_current_date(company, plan, date)
      create_contract_for_date(company, plan, date)
    end

    def create_contract_for_old_date(company, plan, base_date)
      old_date = base_date - 1.year
      old_date -= 2.months
      create_contract_for_date(company, plan, old_date)
    end

    def create_contract_for_date(company, plan, date)
      create_period_and_contract_for_date(company, plan, date)
    end
  end
end
