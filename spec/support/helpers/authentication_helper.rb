module Helpers
  module AuthenticationHelper
    def sign_in(user = double('user'))
      if user.nil?
        mock_user_with_error
      else
        mock_user_with_success(user)
      end
    end

    private

    def mock_user_with_error
      allow(request.env['warden']).to receive(:authenticate!).and_throw(:warden, scope: :user)
      allow(controller).to receive(:current_user).and_return(nil)
    end

    def mock_user_with_success(user)
      allow(request.env['warden']).to receive(:authenticate!).and_return(user)
      allow(controller).to receive(:current_user).and_return(user)
    end
  end
end
