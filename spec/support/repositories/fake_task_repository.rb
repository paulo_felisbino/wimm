module Repositories
  class FakeTaskRepository
    def initialize; end

    def find_raw_data_source(*)
      [
        ['Total horas', '', '', '106:30:00', '', '', ''],
        ['Dia', 'Início', 'Fim', 'Total horas', 'Cliente', 'Projeto', 'Task'],
        ['1', '10:00:00', '12:00:00', '2:00:00', 'Ticket', 'Ticket_Lead Machine', 'Criação de consumers']
      ]
    end
  end
end
