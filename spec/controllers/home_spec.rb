require 'rails_helper'
include ActionCable::TestHelper

RSpec.describe HomeController do
  describe '#update_dashboard' do
    context 'generates an object for broadcast dashboard info' do
      before(:each) do
        @company = create_company
        @user = create_user(@company)
        @plan = create_plan

        @date = DateTime.now
        @period = create_period(@date)
        @contract = create_contract(@company, @plan, @period)
        @task = create_task(@date, @contract)

        sign_in @user
        allow(controller).to receive(:current_user).and_return(@user)
      end

      it 'with all contracts information' do
        date_info = { month: @date.month, year: @date.year }
        contracts = [@contract]
        dashboard_presenter = DashboardPresenter.new(contracts, date_info, contracts)
        message = 'This is a summary for all your contracts this month. Use the awesome filter on your right to be more specific on what you see!'
        charts_objects = {
          pie_chart_contracts: [{
            company: @company.name, plan: @plan.name, period: { month: @period.month, year: @period.year },
            hours_hired: @contract.hours_hired, hours_consumed: @contract.hours_consumed, hours_left: @contract.hours_left
          }],
          bar_chart_contracts: [{
            company: @company.name, plan: @plan.name, period: { month: @period.month, year: @period.year },
            hours_hired: @contract.hours_hired, hours_consumed: @contract.hours_consumed, hours_left: @contract.hours_left
          }]
        }
        broadcast_object = DashboardService.generate_broadcast_object(dashboard_presenter, message, charts_objects)

        assert_broadcast_on("dashboard_#{@user.id}", broadcast_object) do
          post :update_dashboard
        end
      end

      it 'with a selected contract id' do
        date_info = { month: @date.month, year: @date.year }
        contracts = [@contract]
        dashboard_presenter = DashboardPresenter.new(contracts, date_info, contracts, @contract.id)
        message = "This is a summary for contract #{@plan.name} for this month. Use the awesome filter on your right to change what you see!"
        charts_objects = {
          pie_chart_contracts: [{
            company: @company.name, plan: @plan.name, period: { month: @period.month, year: @period.year },
            hours_hired: @contract.hours_hired, hours_consumed: @contract.hours_consumed, hours_left: @contract.hours_left
          }],
          bar_chart_contracts: [{
            company: @company.name, plan: @plan.name, period: { month: @period.month, year: @period.year },
            hours_hired: @contract.hours_hired, hours_consumed: @contract.hours_consumed, hours_left: @contract.hours_left
          }]
        }
        broadcast_object = DashboardService.generate_broadcast_object(dashboard_presenter, message, charts_objects)

        assert_broadcast_on("dashboard_#{@user.id}", broadcast_object) do
          post :update_dashboard, params: { contract: @contract.id }
        end
      end
    end

    context 'generates an empty object for broadcast dashboard info' do
      it 'with no contracts related at all' do
        date = DateTime.now
        company = create_company
        user = create_user(company)

        sign_in user
        allow(controller).to receive(:current_user).and_return(user)

        date_info = { month: date.month, year: date.year }
        contracts = []
        dashboard_presenter = DashboardPresenter.new(contracts, date_info, contracts)
        message = "You currently have no contracts with us this month. Let's fix that!"
        charts_objects = {
          pie_chart_contracts: [],
          bar_chart_contracts: []
        }
        broadcast_object = DashboardService.generate_broadcast_object(dashboard_presenter, message, charts_objects)

        assert_broadcast_on("dashboard_#{user.id}", broadcast_object) do
          post :update_dashboard
        end
      end
    end
  end
end
