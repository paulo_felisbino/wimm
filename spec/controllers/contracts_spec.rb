require 'rails_helper'

RSpec.describe ContractsController do
  include Helpers::ContractsHelper

  describe '#index' do
    context 'assigns a contract presenter object to be used on index view' do
      before(:each) do
        base_objects = create_base_objects
        @contract = create_contract_for_current_date(base_objects[:company], base_objects[:plan], base_objects[:date])
        @contract2 = create_contract_for_old_date(base_objects[:company], base_objects[:plan], base_objects[:date])

        sign_in_user_and_mock_current_user_from_controller(base_objects[:user], controller)
      end

      context 'given scope :all' do
        before(:each) do
          get :index, params: { scope: 'all' }
          @contract_presenter = assigns(:contract_presenter)
        end

        it 'generates a contract presenter' do
          expect(@contract_presenter).to be_a_kind_of(ContractPresenter)
        end

        it 'populates the presenter with all contracts' do
          expect(@contract_presenter.contracts.size).to eq(2)
        end

        it 'includes the contract from current period' do
          expect(@contract_presenter.contracts.include?(@contract)).to be true
        end

        it 'includes the contract from old period' do
          expect(@contract_presenter.contracts.include?(@contract2)).to be true
        end
      end

      context 'given default scope (:current_month)' do
        before(:each) do
          get :index
          @contract_presenter = assigns(:contract_presenter)
        end

        it 'generates a contract presenter' do
          expect(@contract_presenter).to be_a_kind_of(ContractPresenter)
        end

        it 'populates the presenter with 1 contract' do
          expect(@contract_presenter.contracts.size).to eq(1)
        end

        it 'includes the contract from current period' do
          expect(@contract_presenter.contracts.include?(@contract)).to be true
        end

        it 'does not include the contract from old period' do
          expect(@contract_presenter.contracts.include?(@contract2)).to be false
        end
      end
    end
  end
end
