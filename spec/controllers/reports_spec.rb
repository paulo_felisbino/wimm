require 'rails_helper'

RSpec.describe ReportsController do
  include Helpers::ReportsHelper

  describe '#index' do
    it 'returns @report with a default report object' do
      base_objects = create_base_objects_for_index
      sign_in_user_and_mock_current_user_from_controller(base_objects[:user], controller)
      get :index

      expect(assigns(:report)).to be_a_kind_of(Objects::ReportObject)
    end
  end

  describe '#create' do
    context 'returns @locals with all contracts from a given period of the same year' do
      before(:each) do
        @base_objects = create_base_objects_for_create
        sign_in_user_and_mock_current_user_from_controller(@base_objects[:user], controller)
        @objects_for_post_and_expectations = create_objects_for_post_and_expectations(@base_objects[:date])
        post :create, params: @objects_for_post_and_expectations[:params]
        @locals = assigns(:locals)
      end

      it 'contains correct start_date' do
        expect(@locals[:start_date]).to eq(@objects_for_post_and_expectations[:start_date])
      end

      it 'contains correct end_date' do
        expect(@locals[:end_date]).to eq(@objects_for_post_and_expectations[:end_date])
      end

      it 'contains contracts properly filled' do
        expect(@locals[:contracts]).not_to be_empty
      end

      it 'contains corresponding contracts' do
        index = @objects_for_post_and_expectations[:date_group]
        expect(@locals[:contracts][index]).to include(@base_objects[:contract])
      end
    end

    context 'returns @locals with all contracts from a given period of the different years' do
      before(:each) do
        @base_objects = create_base_objects_for_create
        sign_in_user_and_mock_current_user_from_controller(@base_objects[:user], controller)
        @objects_for_post_and_expectations = create_objects_for_post_and_expectations_with_old_date(@base_objects[:date])
        post :create, params: @objects_for_post_and_expectations[:params]
        @locals = assigns(:locals)
      end

      it 'contains correct start_date' do
        expect(@locals[:start_date]).to eq(@objects_for_post_and_expectations[:start_date])
      end

      it 'contains correct end_date' do
        expect(@locals[:end_date]).to eq(@objects_for_post_and_expectations[:end_date])
      end

      it 'contains contracts properly filled' do
        expect(@locals[:contracts]).not_to be_empty
      end

      it 'contains corresponding contracts' do
        index = @objects_for_post_and_expectations[:date_group]
        expect(@locals[:contracts][index]).to include(@base_objects[:contract])
      end
    end
  end

  describe '#export_xls' do
    context 'returns @locals with all contracts from a given period' do
      before(:each) do
        @base_objects = create_base_objects_for_export_xls
        sign_in_user_and_mock_current_user_from_controller(@base_objects[:user], controller)
        @objects_for_post_and_expectations_for_export = create_objects_for_post_and_expectations_for_export(@base_objects[:date])
        post :export_xls, params: @objects_for_post_and_expectations_for_export[:params]
        @locals = assigns(:locals)
      end

      it 'contains correct start_date' do
        expect(@locals[:start_date]).to eq(@objects_for_post_and_expectations_for_export[:start_date])
      end

      it 'contains correct end_date' do
        expect(@locals[:end_date]).to eq(@objects_for_post_and_expectations_for_export[:end_date])
      end

      it 'contains contracts properly filled' do
        expect(@locals[:contracts]).not_to be_empty
      end

      it 'contains corresponding contracts' do
        index = @objects_for_post_and_expectations_for_export[:date_group]
        expect(@locals[:contracts][index]).to include(@base_objects[:contract])
      end
    end
  end
end
