require 'rails_helper'

RSpec.describe TasksController do
  include Helpers::TasksHelper

  describe '#index' do
    context 'assigns a task presenter object to be used on index view' do
      before(:each) do
        base_objects = create_base_objects
        @task = create_task_for_current_date(base_objects[:company], base_objects[:plan], base_objects[:date])
        @task2 = create_task_for_old_date(base_objects[:company], base_objects[:plan], base_objects[:date])

        sign_in_user_and_mock_current_user_from_controller(base_objects[:user], controller)
      end

      context 'given scope :all' do
        before(:each) do
          get :index, params: { scope: 'all' }
          @task_presenter = assigns(:task_presenter)
        end

        it 'generates a task presenter' do
          expect(@task_presenter).to be_a_kind_of(TaskPresenter)
        end

        it 'populates the presenter with all tasks' do
          expect(@task_presenter.tasks.size).to eq(2)
        end

        it 'includes the task from current period' do
          expect(@task_presenter.tasks.include?(@task)).to be true
        end

        it 'includes the task from old period' do
          expect(@task_presenter.tasks.include?(@task2)).to be true
        end
      end

      context 'given default scope (:current_month)' do
        before(:each) do
          get :index
          @task_presenter = assigns(:task_presenter)
        end

        it 'generates a task presenter' do
          expect(@task_presenter).to be_a_kind_of(TaskPresenter)
        end

        it 'populates the presenter with 1 task' do
          expect(@task_presenter.tasks.size).to eq(1)
        end

        it 'includes the task from current period' do
          expect(@task_presenter.tasks.include?(@task)).to be true
        end

        it 'does not include the task from old period' do
          expect(@task_presenter.tasks.include?(@task2)).to be false
        end
      end
    end
  end
end
