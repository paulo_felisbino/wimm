require 'rails_helper'

RSpec.describe Objects::ReportObject do
  it 'has a valid factory' do
    contract = FactoryGirl.build(:report)
    expect(contract.valid?).to be true
  end

  it 'is invalid without a start_date' do
    contract = FactoryGirl.build(:report, start_date: nil)
    expect(contract.valid?).to be false
  end

  it 'is invalid without a end_date' do
    contract = FactoryGirl.build(:report, start_date: nil)
    expect(contract.valid?).to be false
  end

  describe '#initialize' do
    it 'initializes correctly given hash with two dates' do
      obj = Objects::ReportObject.new(start_date: Date.today, end_date: Date.today)
      expect(obj.start_date).to eq(Date.today)
      expect(obj.end_date).to eq(Date.today)
    end
  end

  describe '#persisted?' do
    it 'returns false' do
      obj = Objects::ReportObject.new(start_date: Date.today, end_date: Date.today)
      expect(obj.persisted?).to be false
    end
  end
end
