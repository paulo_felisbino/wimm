require 'rails_helper'

RSpec.describe Updaters::TaskUpdater do
  include Helpers::TaskUpdaterHelper

  before(:each) do
    @base_objects = create_base_objects
    @task_repository = @base_objects[:task_repository]
    @task_updater = Updaters::TaskUpdater.new(@base_objects[:period].month, @base_objects[:period].year, @task_repository)
  end

  describe '#initialize' do
    context 'set initial variables to be used' do
      it 'populates task repository object' do
        expect(@task_updater.task_repository).to eql(@task_repository)
      end

      it 'populates period information object ' do
        expected_period_information = { current_period: @base_objects[:period], readable_month: 'Fevereiro' }
        expect(@task_updater.month_info).to eql(expected_period_information)
      end
    end
  end

  describe '#update_tasks' do
    context 'updates tasks from users' do
      it 'has 2 tasks before update' do
        all_tasks_size_before_update = Task.all.count
        expect(all_tasks_size_before_update).to eql(2)
      end

      context 'after run the task update service' do
        before(:each) do
          @task_updater.update_tasks
          @all_tasks_after_update = Task.all
        end

        it 'has 1 task after update' do
          expect(@all_tasks_after_update.size).to eql(1)
        end

        it 'has 1 task after update with description "Criação de consumers"' do
          expect(@all_tasks_after_update.first.description).to eql('Criação de consumers')
        end
      end
    end
  end
end
