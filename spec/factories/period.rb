FactoryGirl.define do
  factory :period do
    month 1
    year 2018
  end
end
