FactoryGirl.define do
  factory :user do
    name 'Paulo'
    email 'paulo@vizir.com.br'
    password 'v1z12010'
    spreadsheet_key '1Mowqbpzwc4JV2bbAAtsnOuHoukTEoCUc9YMmwdZdElk'
    company { association(:company) }
  end
end
