FactoryGirl.define do
  factory :report, class: Objects::ReportObject do
    start_date Date.today.beginning_of_month
    end_date Date.today.end_of_month
  end
end
