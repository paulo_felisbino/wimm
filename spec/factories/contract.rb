FactoryGirl.define do
  factory :contract do
    plan { association(:plan) }
    period { association(:period) }
    company { association(:company) }
    hours_hired 40
    hours_consumed 10
    hours_left 30
  end
end
