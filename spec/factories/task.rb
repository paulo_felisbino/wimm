FactoryGirl.define do
  factory :task do
    description 'A task'
    duration 10
    task_date '2017-10-09'
    contract { association(:contract) }
    assigned_user_id 1
  end
end
