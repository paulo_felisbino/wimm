Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'sessions' }
  authenticate :user do
    require 'sidekiq/web'
    mount Sidekiq::Web => '/sidekiq'
    mount ActionCable.server => '/cable'

    root 'home#index'
    resources :contracts, only: %i[index]
    resources :tasks, only: %i[index]

    resources :reports, only: %i[index]
    get '/reports/reports/new' => 'reports#new', as: :new_report
    post '/reports/reports' => 'reports#create', as: :create_report
    post 'reports/export_xls' => 'reports#export_xls', as: :export_report_as_xls

    post 'dashboard/update_dashboard' => 'home#update_dashboard', as: :update_dashboard
  end
end
