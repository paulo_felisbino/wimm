# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
Rails.application.config.assets.precompile += [
  'login.css',
  'style.css',
  'reports.css',
  'home.css',
  'tasks.css',
  'contracts.css',
  'scopes.css',
  'menu.css',

  'custom_bootstrap.css',
  'bootstrap_and_overrides.css',
  'bootstrap-datetimepicker.min.css',
  'dataTable.css',
  'sb-admin-2.css',
  'dataTable/dataTables.bootstrap.min.css',
  'dataTable/dataTables.responsive.css',
  'sb-admin-2/metisMenu.min.css',
  'sb-admin-2/sb-admin-2.min.css',

  'login.js',
  'home.js',
  'contracts.js',
  'tasks.js',
  'reports.js',

  'custom_bootstrap.js',
  'dataTable.js',
  'charts.js',
  'sb-admin-2.js',
  'jquery.backstretch.min.js',
  'bluebird.min.js',
  'moment.min.js',
  'popper.js',
  'bootstrap-datetimepicker.min.js',
  'dataTable/jquery.dataTables.min.js',
  'dataTable/dataTables.bootstrap.min.js',
  'dataTable/dataTables.responsive.js',
  'flot/excanvas.min.js',
  'flot/jquery.flot.js',
  'flot/jquery.flot.pie.js',
  'flot/jquery.flot.resize.js',
  'flot/jquery.flot.time.js',
  'flot/jquery.flot.tooltip.min.js',
  'flot/jquery.flot.categories.js',
  'flot/jquery.flot.orderBars.js',
  'sb-admin-2/metisMenu.min.js',
  'sb-admin-2/sb-admin-2.min.js'
]
