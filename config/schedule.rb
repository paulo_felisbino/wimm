require 'tzinfo'

set :output, '/var/log/cron.log'
set :environment, ENV['RAILS_ENV']

def sp_time(time)
  TZInfo::Timezone.get('America/Sao_Paulo').local_to_utc(Time.parse(time))
end

every :day, at: sp_time('23:00') do
  runner 'UpdateTasksJob.perform_later'
end
