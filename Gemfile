source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '5.1.4'
# Use mysql as the database for Active Record
gem 'mysql2'
# Use SCSS for stylesheets
# gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier'
# Use CoffeeScript for .js.coffee assets and views
# gem 'coffee-rails', '~> 4.0.0'
# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer',  platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.0'
# bundle exec rake doc:rails generates the API under doc/api.
# gem 'sdoc', '~> 0.4.0', group: :doc

gem 'font-awesome-rails'

gem 'twitter-bootstrap-rails'

gem 'devise', '~> 4.3.0'

gem 'simple_form', '~> 3.5.0'

# use with rails action cable
gem 'redis', '~> 3.0'

# puma server
gem 'puma', '~> 3.11.2'

# generate xls
gem 'axlsx', '~> 2.0.1'
gem 'axlsx_rails', '~> 0.5.1'
gem 'axlsx_styler', '~> 0.1.7'

# ensure i18n quality/use
gem 'i18n-tasks', '~> 0.9.20'

# google drive integration
gem 'google_drive', '~> 2.1.8'

# natural language date parser
gem 'chronic', '~> 0.10.2'

# queue jobs
gem 'sidekiq', '~> 5.1.1'

# add jobs to cron
gem 'whenever', '~> 0.10.0'

group :development, :test do
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-commands-rspec'

  # linter ruby
  gem 'rubocop', '~> 0.50.0'

  # linter js
  gem 'jshint', '~> 1.5.0'

  # debug
  gem 'better_errors', '~> 2.3'
  gem 'binding_of_caller', '~> 0.7.2'
  gem 'pry'
  gem 'pry-rails'

  # gem 'web-console', '~> 2.0'

  # testing
  gem 'action-cable-testing', '~> 0.2.0'
  gem 'capybara', '~> 2.15.3'
  gem 'database_cleaner', '~> 1.6.2'
  gem 'factory_girl_rails', '~> 4.8.0'
  gem 'fuubar', '~> 2.3.1'
  gem 'rails-controller-testing', '~> 1.0.2'
  gem 'rspec-json_expectations', '~> 2.1.0'
  gem 'rspec-rails', '~> 3.6'

  # gem 'faker', '~> 1.8.4'

  # gem 'quiet_assets'

  # code coverage
  gem 'simplecov', '~> 0.15.1'

  # code quality check
  gem 'rails_best_practices', '~> 1.19.0'
  gem 'sandi_meter', '~> 1.2.0'
end

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use unicorn as the app server
# gem 'unicorn'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]
