## Guia de Commit ##

Padronizando a forma de escrita dos commits, para criar **mensagens mais legí­veis** e que transmitem facilmente o histórico do projeto.

* Escreva sempre título e uma mensagem explicando o que foi feito.
* Idioma padronizado: **Inglês**.

### Formato da mensagem do commit ###

````
[Tag] Título do Commit

Mensagem do commit. Geralmente explicando o que foi alterado,
removido ou adicionado e possÃ­veis detalhes de implementaÃ§Ã£o
que possam ser usados pela equipe em desenvolvimentos futuros.
````

### Tabela de Tags ###

* **Feat:** Uma nova funcionalidade
* **Fix:** Correção de algum bug
* **Style:** Mudanças que não alteram o significado do código (white-space, formatação, ponto-e-virgula faltando...)
* **Refactor:** Alteração do código que não corrige ou adiciona nada.
* **Layout:** Alterações de Front-end (layout)
* **Test:** Adicionando teste
* **Docs:** Alteração em documentação
* **Git:** Alteração em arquivos para versionamento
* **Infra:** Alteração em arquivos do Docker
* **Perf:** Alterações de código para melhoria de performance
