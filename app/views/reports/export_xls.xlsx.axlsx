require 'axlsx_styler'
wb = xlsx_package.workbook

wb.styles do |s|
  bold = { b: true }
  centered = { alignment: { horizontal: :center } }
  defaults =  { :style => :thin, :color => "000000" }
  borders = Hash.new do |hash, key|
    hash[key] = s.add_style :border => defaults.merge( { :edges => key.to_s.split('_').map(&:to_sym) } )
  end

  custom_styles = {
    bold: bold,
    centered: centered,
    borders: borders
  }

  wb.add_worksheet(name: t('reports.export_xls.xlsx.tab_title')) do |sheet|

    sheet.column_widths 18, 18, 18, 18, 18

    sheet.add_row
    render 'reports/export/report_period', locals: { sheet: sheet, style: s, start_date: @locals[:start_date], end_date: @locals[:end_date] }

    sheet.add_row
    render(
      partial: 'reports/export/report_period_summary',
      locals: { sheet: sheet, s: s, custom_styles: custom_styles, contracts_groups: @locals[:contracts], total_hours: @locals[:contract_summed_hours] }
    )

    sheet.add_row
    render 'reports/export/report_period_details', locals: { sheet: sheet, style: s, custom_styles: custom_styles, contracts: @locals[:contracts] }
  end
end
