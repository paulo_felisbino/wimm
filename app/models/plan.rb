class Plan < ApplicationRecord
  has_many :contracts
  has_many :periods, through: :contracts
  has_many :companies, through: :contracts

  validates :name, presence: true
end
