class Task < ApplicationRecord
  belongs_to :contract
  delegate :company, to: :contract
  delegate :plan, to: :contract

  validates :description, :contract_id, presence: true

  after_save :update_contract_hours
  after_destroy :update_contract_hours

  private

  def update_contract_hours
    # triggers contract methods that update hours
    contract.save
  end
end
