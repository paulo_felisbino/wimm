class Company < ApplicationRecord
  has_many :users
  has_many :contracts
  has_many :plans, through: :contracts

  validates :name, presence: true
end
