class Contract < ApplicationRecord
  belongs_to :company
  belongs_to :plan
  belongs_to :period
  has_many :tasks, dependent: :destroy

  validates :company_id, :plan_id, :period_id, presence: true

  before_save :set_hours

  scope :from_company, ->(company_id) { where(company_id: company_id) }

  private

  def set_hours
    sum_consumed_hours
    calculate_left_hours
  end

  def sum_consumed_hours
    consumed_hours_in_seconds = 0
    tasks.all.each do |task|
      consumed_hours_in_seconds = calculate_consumed_hours(consumed_hours_in_seconds, task.duration)
    end
    self.hours_consumed = DecimalToTimeFormatter.convert_seconds_to_big_decimal(consumed_hours_in_seconds)
  end

  def calculate_consumed_hours(consumed_hours_in_seconds, duration)
    duration_in_seconds = DecimalToTimeFormatter.convert_big_decimal_to_seconds(duration)
    consumed_hours_in_seconds += duration_in_seconds
    consumed_hours_in_seconds
  end

  def calculate_left_hours
    hours_hired_in_seconds = DecimalToTimeFormatter.convert_big_decimal_to_seconds(hours_hired)
    hours_consumed_in_seconds = DecimalToTimeFormatter.convert_big_decimal_to_seconds(hours_consumed)
    hours_left_in_seconds = hours_hired_in_seconds - hours_consumed_in_seconds
    self.hours_left = DecimalToTimeFormatter.convert_seconds_to_big_decimal(hours_left_in_seconds)
  end
end
