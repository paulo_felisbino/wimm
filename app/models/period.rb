class Period < ApplicationRecord
  has_many :contracts
  has_many :plans, through: :contracts
  validates :month, :year, presence: true
end
