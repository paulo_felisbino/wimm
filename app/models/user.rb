class User < ApplicationRecord
  belongs_to :company

  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

  validates :name, :email, :password, :company_id, presence: true
end
