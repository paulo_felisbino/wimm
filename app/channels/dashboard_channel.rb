class DashboardChannel < ApplicationCable::Channel
  def subscribed
    create_main_notification_and_loading_streams
    create_panels_streams
    create_chart_stream
  end

  def send_to_dashboard_components(data)
    show_loading
    broadcast_to_components(data)
    hide_loading
  end

  private

  def create_main_notification_and_loading_streams
    stream_from "dashboard_#{current_user.id}"
    stream_from "notification_#{current_user.id}"
    stream_from "loading_#{current_user.id}"
  end

  def create_panels_streams
    stream_from "number_of_plans_#{current_user.id}"
    stream_from "number_of_hired_hours_#{current_user.id}"
    stream_from "number_of_consumed_hours_#{current_user.id}"
    stream_from "number_of_remaining_hours_#{current_user.id}"
  end

  def create_chart_stream
    stream_from "charts_#{current_user.id}"
  end

  def show_loading
    data = { stream: 'loading', action: :show }
    ActionCable.server.broadcast "loading_#{current_user.id}", data
  end

  def hide_loading
    data = { stream: 'loading', action: :hide }
    ActionCable.server.broadcast "loading_#{current_user.id}", data
  end

  def broadcast_to_components(data)
    broadcast_notification(data)
    broadcast_numbers(data)
    broadcast_charts(data)
  end

  def broadcast_notification(data)
    ActionCable.server.broadcast "notification_#{current_user.id}", data.merge(stream: 'notification')
  end

  def broadcast_numbers(data)
    ActionCable.server.broadcast "number_of_plans_#{current_user.id}", data.merge(stream: 'number_of_plans')
    ActionCable.server.broadcast "number_of_hired_hours_#{current_user.id}", data.merge(stream: 'number_of_hired_hours')
    ActionCable.server.broadcast "number_of_consumed_hours_#{current_user.id}", data.merge(stream: 'number_of_consumed_hours')
    ActionCable.server.broadcast "number_of_remaining_hours_#{current_user.id}", data.merge(stream: 'number_of_remaining_hours')
  end

  def broadcast_charts(data)
    ActionCable.server.broadcast "charts_#{current_user.id}", data.merge(stream: 'charts')
  end
end
