class ContractService
  class << self
    def sum_contracts_consumed_hours(contracts)
      contracts_consumed_hours = 0
      contracts.each do |contract|
        contracts_consumed_hours += contract.hours_consumed
      end
      contracts_consumed_hours
    end

    def get_contracts_from_company(company_id)
      Contract.includes(:company, :plan, :period).from_company(company_id)
    end

    def get_contracts_from_company_filtered_by_month_and_year(company_id, month, year)
      contracts = Contract.from_company(company_id)
      filtered_contracts = filter_contracts_by_month_and_year(contracts, month, year)
      filtered_contracts
    end

    def get_contracts_with_tasks_from_company_filtered_by_month_and_year(company_id, month, year)
      contracts = get_contracts_with_tasks_from_company(company_id)
      filtered_contracts = filter_contracts_by_month_and_year(contracts, month, year)
      filtered_contracts
    end

    def get_contracts_from_company_filtered_by_year(company_id, year)
      contracts = Contract.from_company(company_id)
      filtered_contracts = filter_contracts_by_year(contracts, year)
      filtered_contracts
    end

    private

    def get_contracts_with_tasks_from_company(company_id)
      Contract.includes(:company, :plan, :period, :tasks).from_company(company_id)
    end

    def filter_contracts_by_month_and_year(contracts, month, year)
      period = Period.where(month: month, year: year).first
      filtered_contracts = contracts.where(period: period)
      filtered_contracts
    end

    def filter_contracts_by_year(contracts, year)
      periods = Period.where(year: year)
      filtered_contracts = contracts.where(period: periods)
      filtered_contracts
    end
  end
end
