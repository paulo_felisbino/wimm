class ReportService
  class << self
    def get_contracts_for_date_range(start_date, end_date, user)
      range_month_and_year = get_range_of_month_and_year(start_date, end_date)
      get_contracts_for_range_of_month_and_year(range_month_and_year, user)
    end

    def sum_contracts_hours(contracts)
      summed_hours_in_seconds_per_period = sum_hours_per_period_in_seconds(contracts)
      total_summed_hours_in_seconds = sum_total_hours_in_seconds(summed_hours_in_seconds_per_period)
      generate_total_summed_hours_object(total_summed_hours_in_seconds)
    end

    private

    def get_range_of_month_and_year(start_date, end_date)
      args = Utilities::ReportService::Main.qualify_params(start_date, end_date)
      Utilities::ReportService::Main.generate_range_of_dates(args)
    end

    def get_contracts_for_range_of_month_and_year(range_month_and_year, user)
      contracts = {}
      range_month_and_year.each do |month_and_year|
        contracts = Utilities::ReportService::Main.populate_contracts_response(contracts, month_and_year, user)
      end
      contracts
    end

    def sum_hours_per_period_in_seconds(contracts)
      response = {}
      contracts.each do |contract_group|
        response = Utilities::ReportService::Main.sum_hours_and_update_response(response, contract_group) if !contract_group.last.empty?
      end
      response
    end

    def sum_total_hours_in_seconds(summed_hours_in_seconds_per_period)
      total_hours_in_seconds = { hours_hired: 0, hours_consumed: 0, hours_left: 0 }
      summed_hours_in_seconds_per_period.each do |contract_group|
        total_hours_in_seconds = Utilities::ReportService::Main.update_total_hours_in_seconds(total_hours_in_seconds, contract_group.last)
      end
      Utilities::ReportService::Main.generate_total_hours_object(total_hours_in_seconds)
    end

    def generate_total_summed_hours_object(total_hours_in_seconds)
      {
        total_hours_hired: Utilities::ReportService::Main.convert_hours_in_seconds_to_decimal(total_hours_in_seconds[:total_hours_hired_in_seconds]),
        total_hours_consumed: Utilities::ReportService::Main.convert_hours_in_seconds_to_decimal(total_hours_in_seconds[:total_hours_consumed_in_seconds]),
        total_hours_left: Utilities::ReportService::Main.convert_hours_in_seconds_to_decimal(total_hours_in_seconds[:total_hours_left_in_seconds])
      }
    end
  end
end
