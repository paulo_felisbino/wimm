module Repositories
  class TaskRepository
    attr_accessor :session

    def initialize(session = nil)
      @session = session || GoogleDrive::Session.from_config('config/google_drive.json')
    end

    def find_raw_data_source(spreadsheet_key, worksheet_title)
      worksheet = get_worksheet_to_read(spreadsheet_key, worksheet_title)
      worksheet.rows
    end

    private

    def get_worksheet_to_read(spreadsheet_key, worksheet_title)
      session.spreadsheet_by_key(spreadsheet_key).worksheet_by_title(worksheet_title)
    end
  end
end
