module Updaters
  class TaskUpdater
    attr_accessor :task_repository, :month_info

    def initialize(month, year, task_repository = nil)
      @task_repository = task_repository || Repositories::TaskRepository.new
      @month_info = Utilities::TaskUpdater::MonthParser.get_month_information_to_read(month, year)
    end

    def update_tasks
      users = get_users_with_spreadsheet_key
      users.each do |user|
        update_tasks_from_user(user)
      end
    end

    private

    def get_users_with_spreadsheet_key
      User.where.not(spreadsheet_key: nil)
    end

    def update_tasks_from_user(user)
      rows = task_repository.find_raw_data_source(user.spreadsheet_key, month_info[:readable_month])
      clean_month_data_before_start_reading(month_info[:current_period], user)
      create_task_data_from_raw_source(rows, month_info[:current_period], user)
    end

    def clean_month_data_before_start_reading(period, user)
      Utilities::TaskUpdater::DataParser.clear_all_tasks_for_period_from_user(period, user)
    end

    def create_task_data_from_raw_source(rows, period, user)
      rows.each do |row|
        next if row_is_useless?(row)
        qualified_data = Utilities::TaskUpdater::DataParser.parse_data(row, period, user)
        Utilities::TaskUpdater::DataParser.create_task(qualified_data)
      end
    end

    def row_is_useless?(row)
      row[0] == 'Total horas' || row[0] == 'Dia' || row[0] == ''
    end
  end
end
