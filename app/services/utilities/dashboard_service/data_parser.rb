module Utilities
  module DashboardService
    class DataParser
      class << self
        def parse_broadcast_object(dashboard_presenter, message, charts_objects)
          broadcast_object = { stream: 'dashboard' }
          broadcast_object = broadcast_object.merge(generate_notification_object(message))
          broadcast_object = add_numbers_objects(broadcast_object, dashboard_presenter)
          broadcast_object = broadcast_object.merge(generate_charts_object(dashboard_presenter, charts_objects))
          broadcast_object
        end

        private

        def generate_notification_object(message)
          { notification: generate_notification_partial(message) }
        end

        def add_numbers_objects(broadcast_object, dashboard_presenter)
          broadcast_object = broadcast_object.merge(generate_number_of_plans_object(dashboard_presenter.number_plans))
          broadcast_object = broadcast_object.merge(generate_number_of_hired_hours_object(dashboard_presenter.number_hired_hours))
          broadcast_object = broadcast_object.merge(generate_number_of_consumed_hours_object(dashboard_presenter.number_consumed_hours))
          broadcast_object = broadcast_object.merge(generate_number_of_remaining_hours_object(dashboard_presenter))
          broadcast_object
        end

        def generate_number_of_plans_object(number_plans)
          { number_plans: generate_number_of_plans_partial(number_plans) }
        end

        def generate_number_of_hired_hours_object(number_hired_hours)
          { hired_hours: generate_number_of_hired_hours_partial(number_hired_hours) }
        end

        def generate_number_of_consumed_hours_object(number_consumed_hours)
          { consumed_hours: generate_number_of_consumed_hours_partial(number_consumed_hours) }
        end

        def generate_number_of_remaining_hours_object(dashboard_presenter)
          { remaining_hours: generate_number_of_remaining_hours_partial(dashboard_presenter) }
        end

        def generate_charts_object(dashboard_presenter, charts_objects)
          { charts: { data: generate_charts_data_object(charts_objects), partial: generate_charts_partial(dashboard_presenter) } }
        end

        def generate_notification_partial(message)
          ApplicationController.render(partial: '/home/dashboard_notification', locals: { message: message })
        end

        def generate_number_of_plans_partial(number_plans)
          ApplicationController.render(
            partial: '/home/panel_primary', locals: { number: number_plans, text: I18n.t('dashboard.plans.text'), details_url: contracts_uri }
          )
        end

        def generate_number_of_hired_hours_partial(hired_hours)
          ApplicationController.render(
            partial: '/home/panel_secondary', locals: { number: hired_hours, text: I18n.t('dashboard.hired_hours.text'), details_url: contracts_uri }
          )
        end

        def generate_number_of_consumed_hours_partial(consumed_hours)
          ApplicationController.render(
            partial: '/home/panel_secondary', locals: { number: consumed_hours, text: I18n.t('dashboard.consumed_hours.text'), details_url: tasks_uri }
          )
        end

        def generate_number_of_remaining_hours_partial(dashboard_presenter)
          ApplicationController.render(
            partial: '/home/remaining_hours_panel', locals: { dashboard: dashboard_presenter, text: I18n.t('dashboard.remaining_hours.text') }
          )
        end

        def generate_charts_data_object(charts_objects)
          { plans_summary: charts_objects[:pie_chart_contracts], year_details: charts_objects[:bar_chart_contracts] }
        end

        def generate_charts_partial(dashboard_presenter)
          ApplicationController.render(partial: '/home/charts', locals: { dashboard: dashboard_presenter })
        end

        def contracts_uri
          Rails.application.routes.url_helpers.contracts_path(self)
        end

        def tasks_uri
          Rails.application.routes.url_helpers.tasks_path(self)
        end
      end
    end
  end
end
