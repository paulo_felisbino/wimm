module Utilities
  module TaskUpdater
    class MonthParser
      def self.get_month_information_to_read(month, year)
        period = Period.where(month: month, year: year).first
        readable_month = parse_month_as_name(period.month)
        { current_period: period, readable_month: readable_month }
      end

      class << self
        private

        def parse_month_as_name(month)
          index = month - 1
          months_name = %w[Janeiro Fevereiro Março Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro]
          months_name[index]
        end
      end
    end
  end
end
