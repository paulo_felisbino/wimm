module Utilities
  module TaskUpdater
    class DataParser
      def self.clear_all_tasks_for_period_from_user(current_period, user)
        current_period.contracts.each do |contract|
          contract.tasks.each do |task|
            task.destroy if task.assigned_user_id == user.id
          end
        end
      end

      def self.parse_data(row, period, user)
        raw_data = { day: row[0], duration: row[3], company_name: row[4], plan_name: row[5], description: row[6] }
        populate_response_for_parse_data(raw_data, period, user)
      end

      def self.create_task(qualified_data)
        Task.create(
          task_date: qualified_data[:task_date], duration: qualified_data[:duration], description: qualified_data[:description],
          contract: qualified_data[:contract], assigned_user_id: qualified_data[:user_id]
        )
      end

      class << self
        private

        def populate_response_for_parse_data(raw_data, period, user)
          response = {}
          response = response.merge(generate_task_info(raw_data, period))
          response = response.merge(generate_contract_info(raw_data, period))
          response = response.merge(generate_user_info(user))
          response
        end

        def generate_task_info(raw_data, period)
          { task_date: prepare_task_date(raw_data[:day], period.month), duration: prepare_duration(raw_data[:duration]), description: raw_data[:description] }
        end

        def generate_contract_info(raw_data, period)
          company = prepare_company(raw_data[:company_name])
          plan = prepare_plan(raw_data[:plan_name])
          { company: company, plan: plan, contract: prepare_contract(company, plan, period) }
        end

        def generate_user_info(user)
          { user_id: user.id }
        end

        def prepare_task_date(day, current_month)
          today = DateTime.now
          task_date = today.change(month: current_month)
          task_date = task_date.change(day: day.to_i)
          task_date
        end

        def prepare_duration(duration)
          duration_tmp = duration.split(':')
          duration = "#{duration_tmp[0]}.#{duration_tmp[1]}".to_d
          duration
        end

        def prepare_company(name)
          company = Company.find_by_name(name)
          company = Company.create(name: name) if company.nil?
          company
        end

        def prepare_plan(name)
          plan = Plan.find_by_name(name)
          plan = Plan.create(name: name) if plan.nil?
          plan
        end

        def prepare_contract(company, plan, period)
          contract = Contract.where(company: company, plan: plan, period: period).first
          contract = Contract.create(company: company, plan: plan, period: period, hours_hired: 69) if contract.nil?
          contract
        end
      end
    end
  end
end
