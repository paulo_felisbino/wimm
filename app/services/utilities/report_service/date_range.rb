module Utilities
  module ReportService
    class DateRange
      class << self
        def dates_from_different_years?(start_year, end_year)
          start_year.to_i != end_year.to_i
        end

        def calculate_range_for_different_years(date_args)
          range_month_and_year_start = generate_range_for_different_years(date_args[:start][:month], date_args[:start][:year], :start)
          range_month_and_year_end = generate_range_for_different_years(date_args[:end][:month], date_args[:end][:year], :end)
          range_month_and_year_start + range_month_and_year_end
        end

        def generate_range_for_same_year(start_month, end_month, year)
          range_months = (start_month..end_month).to_a
          generate_range_array(range_months, year)
        end

        private

        def generate_range_for_different_years(month, year, type)
          indexes = generate_indexes_for_range_of_months(type, month)
          months = (1..12).to_a
          range_months = months[indexes[:start], indexes[:end]]
          generate_range_array(range_months, year)
        end

        def generate_indexes_for_range_of_months(type, month)
          indexes = { start: month - 1, end: 12 - month + 1 } if type == :start
          indexes = { start: 0, end: month } if type == :end
          indexes
        end

        def generate_range_array(range_months, year)
          range_month_year = []
          range_months.each do |range_month|
            range_month_year.push(month: range_month, year: year)
          end
          range_month_year
        end
      end
    end
  end
end
