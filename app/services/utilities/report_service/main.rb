module Utilities
  module ReportService
    class Main
      class << self
        def qualify_params(start_date, end_date)
          {
            start: { month: start_date.month, year: start_date.year },
            end: { month: end_date.month, year: end_date.year }
          }
        end

        def generate_range_of_dates(date_args)
          if Utilities::ReportService::DateRange.dates_from_different_years?(date_args[:start][:year], date_args[:end][:year])
            return Utilities::ReportService::DateRange.calculate_range_for_different_years(date_args)
          end
          Utilities::ReportService::DateRange.generate_range_for_same_year(date_args[:start][:month], date_args[:end][:month], date_args[:start][:year])
        end

        def populate_contracts_response(contracts, month_and_year, user)
          contracts["#{month_and_year[:month]}_#{month_and_year[:year]}"] = ''
          contracts_from_month =
            ContractService.get_contracts_with_tasks_from_company_filtered_by_month_and_year(user.company_id, month_and_year[:month], month_and_year[:year])
          contracts["#{month_and_year[:month]}_#{month_and_year[:year]}"] = contracts_from_month
          contracts
        end

        def sum_hours_and_update_response(response, contract_group)
          period_group = contract_group.first
          summed_hours_in_seconds = Utilities::ReportService::HoursSum.sum_hours_of_period(summed_hours_in_seconds, contract_group.last)
          Utilities::ReportService::HoursSum.update_response(response, period_group, summed_hours_in_seconds)
        end

        def update_total_hours_in_seconds(total_hours_in_seconds, hours_group)
          total_hours_in_seconds[:hours_hired] += hours_group[:summed_hours_hired]
          total_hours_in_seconds[:hours_consumed] += hours_group[:summed_hours_consumed]
          total_hours_in_seconds[:hours_left] += hours_group[:summed_hours_left]
          total_hours_in_seconds
        end

        def generate_total_hours_object(total_hours_in_seconds)
          {
            total_hours_hired_in_seconds: total_hours_in_seconds[:hours_hired],
            total_hours_consumed_in_seconds: total_hours_in_seconds[:hours_consumed],
            total_hours_left_in_seconds: total_hours_in_seconds[:hours_left]
          }
        end

        def convert_hours_in_seconds_to_decimal(hours_in_seconds)
          DecimalToTimeFormatter.convert_seconds_to_big_decimal(hours_in_seconds)
        end
      end
    end
  end
end
