module Utilities
  module ReportService
    class HoursSum
      class << self
        def sum_hours_of_period(summed_hours_in_seconds, contracts)
          summed_hours_in_seconds = { hours_hired: 0, hours_consumed: 0, hours_left: 0 }
          contracts.each do |contract|
            summed_hours_in_seconds = sum_hours(summed_hours_in_seconds, contract)
          end
          summed_hours_in_seconds
        end

        def update_response(response, period_group, summed_hours_in_seconds)
          response[period_group] = generate_summed_hours_object(summed_hours_in_seconds)
          response
        end

        private

        def sum_hours(summed_hours_in_seconds, contract)
          summed_hours_in_seconds[:hours_hired] += DecimalToTimeFormatter.convert_big_decimal_to_seconds(contract.hours_hired)
          summed_hours_in_seconds[:hours_consumed] += DecimalToTimeFormatter.convert_big_decimal_to_seconds(contract.hours_consumed)
          summed_hours_in_seconds[:hours_left] += DecimalToTimeFormatter.convert_big_decimal_to_seconds(contract.hours_left)
          summed_hours_in_seconds
        end

        def generate_summed_hours_object(summed_hours_in_seconds)
          {
            summed_hours_hired: summed_hours_in_seconds[:hours_hired],
            summed_hours_consumed: summed_hours_in_seconds[:hours_consumed],
            summed_hours_left: summed_hours_in_seconds[:hours_left]
          }
        end
      end
    end
  end
end
