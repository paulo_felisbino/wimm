class TaskService
  class << self
    def get_tasks_from_company(company_id)
      contracts_from_company = ContractService.get_contracts_from_company(company_id)
      tasks = map_contracts_and_populate_tasks(contracts_from_company)
      tasks
    end

    def get_tasks_from_company_filtered_by_month_and_year(company_id, month, year)
      tasks = get_tasks_from_company(company_id)
      filtered_tasks = filter_tasks_by_month_and_year(tasks, month, year)
      filtered_tasks
    end

    private

    def map_contracts_and_populate_tasks(contracts)
      tasks = []
      contracts.each do |contract|
        tasks = populate_tasks_response(tasks, contract.tasks)
      end
      tasks
    end

    def populate_tasks_response(response_tasks, contract_tasks)
      contract_tasks.each do |task|
        response_tasks.push(task)
      end
      response_tasks
    end

    def filter_tasks_by_month_and_year(tasks, month, year)
      task_date_filter = create_date_of_reference_for_task(month, year)
      filtered_tasks = populate_filtered_tasks_response(tasks, task_date_filter)
      filtered_tasks
    end

    def create_date_of_reference_for_task(month, year)
      today = DateTime.now
      task_date = today.change(month: month.to_i)
      task_date = task_date.change(year: year.to_i)
      task_date.to_date
    end

    def populate_filtered_tasks_response(tasks, task_date_filter)
      filtered_tasks = []
      tasks.each do |task|
        filtered_tasks.push(task) if task_in_range?(task_date_filter.beginning_of_month, task_date_filter.end_of_month, task.task_date)
      end
      filtered_tasks
    end

    def task_in_range?(date_start, date_end, task_date)
      (date_start..date_end).cover?(task_date)
    end
  end
end
