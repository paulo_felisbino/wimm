class DashboardService
  class << self
    def update_selected_contracts(contracts, contract_id)
      selected_contracts = contracts
      selected_contracts = contracts.where(id: contract_id) if contract_id.present? && contract_id.to_i > 0
      selected_contracts
    end

    def create_dashboard_message(contract_id, selected_contracts)
      message = I18n.t('dashboard.notification.messages.default')
      message = I18n.t('dashboard.notification.messages.empty') if selected_contracts.empty?
      message = I18n.t('dashboard.notification.messages.single_contract', name: selected_contracts.first.plan.name) if contract_id.to_i > 0
      message
    end

    def generate_contracts_object_for_bar_chart(dashboard_presenter, company_id)
      contracts_from_year = ContractService.get_contracts_from_company_filtered_by_year(company_id, dashboard_presenter.dashboard_year)
      contracts_from_year = update_contracts_for_specific_year(contracts_from_year, dashboard_presenter.selected_contract)
      generate_contracts_object_for_chart(contracts_from_year)
    end

    def generate_contracts_object_for_chart(param_contracts)
      contracts = []
      param_contracts.each do |contract|
        contracts.push(create_contract_hash_object(contract, contract.company, contract.plan, contract.period))
      end
      contracts
    end

    def generate_broadcast_object(dashboard_presenter, message, charts_objects)
      Utilities::DashboardService::DataParser.parse_broadcast_object(dashboard_presenter, message, charts_objects)
    end

    private

    def update_contracts_for_specific_year(contracts_from_year, contract_id)
      if contract_id.to_i > 0
        plan_id = get_plan_id(contract_id)
        contracts_from_year = contracts_from_year.where(plan_id: plan_id)
      end
      contracts_from_year
    end

    def get_plan_id(contract_id)
      contract_from_selected_plan = Contract.find(contract_id)
      contract_from_selected_plan.plan_id
    end

    def create_contract_hash_object(contract, company, plan, period)
      {
        company: company.name, plan: plan.name, period: { month: period.month, year: period.year },
        hours_hired: contract.hours_hired, hours_consumed: contract.hours_consumed, hours_left: contract.hours_left
      }
    end
  end
end
