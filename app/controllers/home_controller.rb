class HomeController < ApplicationController
  def index
    @dashboard = generate_dashboard_presenter(@current_date_info)
  end

  def update_dashboard
    dashboard_presenter = generate_dashboard_presenter(@current_date_info)
    message = generate_message_for_current_dashboard_context(dashboard_presenter)
    charts_objects = generate_charts_objects(dashboard_presenter)
    broadcast_dashboard_update(dashboard_presenter, message, charts_objects)
    head :ok
  end

  private

  def generate_dashboard_presenter(date_info)
    contracts = ContractService.get_contracts_from_company_filtered_by_month_and_year(current_user.company_id, date_info[:month], date_info[:year])
    contract_id = params[:contract]
    selected_contracts = DashboardService.update_selected_contracts(contracts, contract_id)
    return DashboardPresenter.new(contracts, date_info, selected_contracts, contract_id) if contract_id.present?
    DashboardPresenter.new(contracts, date_info, selected_contracts)
  end

  def generate_message_for_current_dashboard_context(dashboard_presenter)
    contract_id = dashboard_presenter.selected_contract
    selected_contracts = dashboard_presenter.selected_contracts
    DashboardService.create_dashboard_message(contract_id, selected_contracts)
  end

  def generate_charts_objects(dashboard)
    {
      pie_chart_contracts: DashboardService.generate_contracts_object_for_chart(dashboard.contracts),
      bar_chart_contracts: DashboardService.generate_contracts_object_for_bar_chart(dashboard, current_user.company_id)
    }
  end

  def broadcast_dashboard_update(dashboard_presenter, message, charts_objects)
    broadcast_object = DashboardService.generate_broadcast_object(dashboard_presenter, message, charts_objects)
    ActionCable.server.broadcast "dashboard_#{current_user.id}", broadcast_object
  end
end
