class SessionsController < Devise::SessionsController
  layout 'login'
  def destroy
    reset_session
    flash[:success] = t('sessions.logout_success')
    redirect_to new_user_session_path
  end
end
