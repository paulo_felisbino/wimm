class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_action :authenticate_user!
  before_action :set_current_date_info

  def set_current_date_info
    today = DateTime.now
    current_month = today.strftime('%m').to_i
    current_year = today.strftime('%Y').to_i
    @current_date_info = { month: current_month, year: current_year }
  end

  def use_scopes
    current_scope = get_current_scope
    scope_params_objects = create_scope_presenter_params
    @scope_presenter = ScopePresenter.new(current_scope, scope_params_objects[:possible_scopes], scope_params_objects[:scopes_paths])
  end

  private

  def get_current_scope
    scopes = get_filter_scopes
    current_scope = scopes[:default]
    params_scope = params[:scope]
    current_scope = params_scope if params_scope.present? && possible_scope?(params_scope)
    current_scope
  end

  def get_filter_scopes
    scopes = {
      all_scopes: %i[current_month all],
      default: :current_month
    }
    scopes
  end

  def possible_scope?(scope)
    scopes = get_filter_scopes
    scopes[:all_scopes].include?(scope.to_sym)
  end

  def create_scope_presenter_params
    scopes = get_filter_scopes
    {
      possible_scopes: scopes[:all_scopes],
      scopes_paths: { all: { path: root_path, text: t('scopes.all') }, current_month: { path: root_path, text: t('scopes.current_month') } }
    }
  end
end
