class ReportsController < ApplicationController
  def index
    @report = Objects::ReportObject.new
  end

  def create
    @locals = generate_report_data
    respond_to do |format|
      format.js
    end
  end

  def export_xls
    @locals = generate_report_data
    filename = "report_#{DateTime.now.to_s(:date_format)}"
    render xlsx: filename, template: 'reports/export_xls.xlsx.axlsx'
  end

  private

  def generate_report_data
    date_info = date_params
    contracts = get_contracts(date_info)
    create_response_object(date_info, contracts)
  end

  def get_contracts(date_info)
    ReportService.get_contracts_for_date_range(date_info[:start_date], date_info[:end_date], current_user)
  end

  def create_response_object(date_info, contracts)
    summed_hours = ReportService.sum_contracts_hours(contracts)
    { start_date: date_info[:start_date], end_date: date_info[:end_date], contracts: contracts, contract_summed_hours: summed_hours }
  end

  def report_params
    params.require(:objects_report_object).permit(:start_date, :end_date, :submit)
  end

  def date_params
    { start_date: report_params[:start_date].to_date, end_date: report_params[:end_date].to_date }
  end
end
