class ContractsController < ApplicationController
  before_action :use_scopes

  def index
    contracts = get_correct_contracts
    customize_scopes_paths
    @contract_presenter = ContractPresenter.new(contracts, @scope_presenter)
  end

  private

  def get_correct_contracts
    is_current_month = @scope_presenter.current_scope.to_sym == :current_month
    return get_contracts_for_current_month(current_user, @current_date_info[:month], @current_date_info[:year]) if is_current_month
    get_default_contracts(current_user)
  end

  def get_contracts_for_current_month(user, month, year)
    ContractService.get_contracts_from_company_filtered_by_month_and_year(user.company_id, month, year)
  end

  def get_default_contracts(user)
    ContractService.get_contracts_from_company(user.company_id)
  end

  def customize_scopes_paths
    contracts_scopes_paths = {
      all: { path: contracts_path(scope: :all), text: t('scopes.all') },
      current_month: { path: contracts_path(scope: :current_month), text: t('scopes.current_month') }
    }
    @scope_presenter.use_scopes_paths(contracts_scopes_paths)
  end
end
