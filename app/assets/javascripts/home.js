$(document).ready(function() {
  function init(){
    addFilterDashboardByContract()
    firstDashboardUpdate()
  }

  function addFilterDashboardByContract(){
    $("#filtered_contract").change(function(){
      $("form#filter_contracts_form").submit()
    })
  }

  function firstDashboardUpdate(){
    $("form#filter_contracts_form").submit()
  }

  init()
});
