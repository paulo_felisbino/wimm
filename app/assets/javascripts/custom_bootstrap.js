//= require popper
//= require twitter/bootstrap
//= require bootstrap-datetimepicker.min

jQuery(function() {
  $("a[rel~=popover], .has-popover").popover();
  $("a[rel~=tooltip], .has-tooltip").tooltip();
});
