//= require cable
//= require dashboard_charts

function updateNotification(partialNotification){
  $('#section_notification').html(partialNotification)
}

function updateNumberOfPlans(partialNumberOfPlans){
  $('#section_number_plans').html(partialNumberOfPlans)
}

function updateNumberOfHiredHours(partialHiredHours){
  $('#section_number_hired_hours').html(partialHiredHours)
}

function updateNumberOfConsumedHours(partialConsumedHours){
  $('#section_number_consumed_hours').html(partialConsumedHours)
}

function updateNumberOfRemainingHours(partialRemainingHours){
  $('#section_number_remaining_hours').html(partialRemainingHours)
}

function updateCharts(chartsInfo){
  var partial = chartsInfo.partial
  var data = chartsInfo.data

  $('#charts').html(partial)
  if( $("#flot-pie-chart").length > 0 ){
    this.App.chartFunctions.populateChartPieData(data.plans_summary)
    .then(function(chartData) {
      return this.App.chartFunctions.generatePieChart(chartData)
    })
  }

  if( $("#flot-bar-chart").length > 0 ){
    this.App.chartFunctions.populateChartBarData(data.year_details)
    .then(function(chartData) {
      return this.App.chartFunctions.generateBarChart(chartData)
    })
  }
}

function toggleLoading(action){
  if(action === "show"){
    $('.loading').css('display','block')
  }
  else {
    $('.loading').css('display','none')
  }
}

this.App.cable.subscriptions.create('DashboardChannel', {
  received: function(message) {
    var stream = message.stream
    if(stream === 'dashboard'){
      this.perform('send_to_dashboard_components', message)
    }
    else if (stream === 'notification') {
      updateNotification(message.notification)
    }
    else if (stream === 'number_of_plans') {
      updateNumberOfPlans(message.number_plans)
    }
    else if (stream === 'number_of_hired_hours') {
      updateNumberOfHiredHours(message.hired_hours)
    }
    else if (stream === 'number_of_consumed_hours') {
      updateNumberOfConsumedHours(message.consumed_hours)
    }
    else if (stream === 'number_of_remaining_hours') {
      updateNumberOfRemainingHours(message.remaining_hours)
    }
    else if (stream === 'charts') {
      updateCharts(message.charts)
    }
    else if (stream === 'loading') {
      toggleLoading(message.action)
    }
  }
});
