$(document).ready(function() {
  $('#tasks-table').DataTable({
    responsive: true,
    order: [[ 2, "desc" ]],
    dom: '<"row"<"col-sm-6 search"f><"col-sm-6 scopes">> <"row"<"col-sm-12"tr>> <"row"<"col-sm-5"i><"col-sm-7"p>>'
  });

  $("div.scopes").html( $(".hidden-scopes").html() )

  $(".scope-item.active-scope a").click(function(e){
  	e.preventDefault()
  })
});
