$(document).ready(function() {
  $('.datepicker').datetimepicker({
    format: 'DD/MM/YYYY'
  })

  $('#generate_report_btn').click(function(e){
    e.preventDefault()
    $('#report_form').submit()
  })

  $('#export_as_xls_btn').click(function(e){
    e.preventDefault()

    var start_date = $('#report_form_start_date').val()
    var end_date = $('#report_form_end_date').val()

    $('#report_export_form_start_date').val(start_date)
    $('#report_export_form_end_date').val(end_date)
    $('#report_export_form').submit()
  })
})
