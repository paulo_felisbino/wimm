module ApplicationHelper
  def class_for_menu(menu_param)
    menu_class = ''
    menu_class = 'active' if menu_is_selected?(menu_param)
    menu_class
  end

  def menu_is_selected?(menu_param)
    request.original_url.include?(menu_param)
  end

  def human_readable_decimal_as_hour(big_decimal)
    DecimalToTimeFormatter.convert_big_decimal_to_hour_format(big_decimal)
  end
end
