module ReportsHelper
  def human_readable_time(month_year)
    args = qualify_params(month_year)
    readable_month = map_month_to_human_readable(args[:month])
    "#{readable_month}/#{args[:year]}"
  end

  def contract_plan_name(plan)
    plan.name
  end

  private

  def qualify_params(raw_params)
    args = raw_params.split('_')
    { month: args.first.to_i, year: args.last.to_i }
  end

  def map_month_to_human_readable(month)
    Date::MONTHNAMES[month]
  end
end
