class ContractPresenter
  attr_reader :contracts, :scope_presenter

  def initialize(contracts, scope_presenter)
    @contracts = contracts
    @scope_presenter = scope_presenter
  end

  def plan_name(plan)
    plan.name
  end

  def reference_date_to_s(period)
    month = adjust_month_to_two_digits(period.month)
    year = period.year
    "#{month}/#{year}"
  end

  private

  def adjust_month_to_two_digits(month)
    month.to_s.rjust(2, '0')
  end
end
