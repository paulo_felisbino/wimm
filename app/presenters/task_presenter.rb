class TaskPresenter
  attr_reader :tasks, :scope_presenter

  def initialize(tasks, scope_presenter)
    @tasks = tasks
    @scope_presenter = scope_presenter
  end

  def task_plan_name(task)
    task.plan.name
  end

  def task_date_to_s(task)
    task.task_date.to_s(:date_format)
  end
end
