class ScopePresenter
  attr_reader :current_scope, :possible_scopes, :scopes_paths

  def initialize(current_scope, possible_scopes, scopes_paths)
    @current_scope = current_scope
    @possible_scopes = possible_scopes
    @scopes_paths = scopes_paths
  end

  def use_scopes_paths(new_scopes_paths)
    @scopes_paths = new_scopes_paths
  end

  def scope_correct_class(param_scope, current_scope)
    scope_class = ''
    scope_class = 'active-scope' if selected_scope?(param_scope, current_scope)
    scope_class
  end

  private

  def selected_scope?(param_scope, current_scope)
    param_scope.to_sym == current_scope.to_sym
  end
end
