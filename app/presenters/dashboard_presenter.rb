class DashboardPresenter
  include ApplicationHelper

  attr_reader :contracts, :current_date_info, :selected_contract, :selected_contracts

  def initialize(contracts, current_date_info, selected_contracts, selected_contract = -1)
    @contracts = contracts
    @current_date_info = current_date_info
    @selected_contract = selected_contract
    @selected_contracts = selected_contracts
  end

  def number_plans
    selected_contracts.count
  end

  def number_hired_hours
    human_readable_decimal_as_hour(calculate_hired_hours)
  end

  def number_consumed_hours
    human_readable_decimal_as_hour(calculate_consumed_hours)
  end

  def number_remaining_hours
    human_readable_decimal_as_hour(calculate_remaining_hours)
  end

  def dashboard_year
    current_date_info[:year]
  end

  def remaining_hours_message_partial_name
    calculate_remaining_hours >= 0 ? 'positive' : 'negative'
  end

  def contracts_for_filter
    filters = [[I18n.t('dashboard.contract_filter.all'), -1]]
    mapped_contracts = contracts.map { |c| [c.plan.name, c.id] }
    filters + mapped_contracts
  end

  private

  def calculate_hired_hours
    selected_contracts.map(&:hours_hired).sum
  end

  def calculate_consumed_hours
    ContractService.sum_contracts_consumed_hours(selected_contracts)
  end

  def calculate_remaining_hours
    calculate_hired_hours - calculate_consumed_hours
  end
end
