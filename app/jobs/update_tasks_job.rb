class UpdateTasksJob < ApplicationJob
  queue_as :default

  def perform
    today = Date.today
    task_repository = Repositories::TaskRepository.new
    task_updater = Updaters::TaskUpdater.new(today.month, today.year, task_repository)
    task_updater.update_tasks
  end
end
