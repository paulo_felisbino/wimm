class CreateContracts < ActiveRecord::Migration[5.1]
  def change
    create_table :contracts do |t|
      t.integer :plan_id, index: true
      t.integer :company_id, index: true
      t.integer :period_id, index: true
      t.decimal :hours_hired, precision: 10, scale: 2
      t.decimal :hours_consumed, precision: 10, scale: 2
      t.decimal :hours_left, precision: 10, scale: 2

      t.timestamps
    end
  end
end
