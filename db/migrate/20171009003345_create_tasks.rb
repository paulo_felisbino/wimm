class CreateTasks < ActiveRecord::Migration[5.1]
  def change
    create_table :tasks do |t|
      t.text :description
      t.decimal :duration, precision: 10, scale: 2
      t.date :task_date
      t.integer :contract_id
      t.integer :assigned_user_id

      t.timestamps
    end
  end
end
