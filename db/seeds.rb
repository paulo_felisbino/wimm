current_month = DateTime.now.month
current_year = DateTime.now.year
last_year = current_year - 1
last_year_month = 10
months = 1..12

# create company
company = Company.create(name: 'Vizir')

# create user
user = User.create(
  name: 'Paulo', email: 'paulo@vizir.com.br', password: 'v1z12010', company_id: company.id,
  spreadsheet_key: '1Mowqbpzwc4JV2bbAAtsnOuHoukTEoCUc9YMmwdZdElk'
)

User.create(
  name: 'Jefferson', email: 'jefferson@vizir.com.br', password: 'v1z12010', company_id: company.id,
  spreadsheet_key: '1gFdgNoT4yYKydwZuTVOnVAs1FRPTq5_zBZOU7lJC2H0'
)

# create plans
plan = Plan.create(name: 'Manutencao')
another_plan = Plan.create(name: 'Sistema')
old_plan = Plan.create(name: 'Melhorias Sanofi')

# create periods
months.each do |month|
  Period.create(month: month, year: current_year)
  Period.create(month: month, year: last_year)
end

# create contracts
current_period = Period.where(month: current_month, year: current_year).first
contract = Contract.create(plan: plan, company: company, period: current_period, hours_hired: 40)
Contract.create(plan: another_plan, company: company, period: current_period, hours_hired: 10)

another_period = Period.where(month: DateTime.now.beginning_of_year.month, year: current_year).first
Contract.create(plan: plan, company: company, period: another_period, hours_hired: 20)

last_year_october_period = Period.where(month: last_year_month, year: last_year).first
old_contract = Contract.create(plan: old_plan, company: company, period: last_year_october_period, hours_hired: 200)

# add tasks to contracts
task_date = DateTime.now
duration = 10
description = 'Uma tarefa'
old_task_date = task_date.change(month: last_year_month)
old_task_date = old_task_date.change(year: last_year)
old_task_date = old_task_date.to_date

Task.create(task_date: task_date, duration: duration, contract: contract, description: description, assigned_user_id: user.id)
Task.create(task_date: old_task_date, duration: duration, contract: old_contract, description: description, assigned_user_id: user.id)
