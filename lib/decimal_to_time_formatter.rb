class DecimalToTimeFormatter
  def self.convert_big_decimal_to_seconds(hours)
    time = convert_big_decimal_to_hour_format(hours)
    get_total_seconds_from_time(time)
  end

  def self.convert_seconds_to_big_decimal(seconds)
    hours_left_time_format = convert_seconds_to_hour_format(seconds)
    hours_left_time_splited = hours_left_time_format.split(':')
    h = hours_left_time_splited.first
    m = hours_left_time_splited.last
    "#{h}.#{m}".to_d
  end

  def self.convert_big_decimal_to_hour_format(big_decimal)
    decimal = big_decimal.to_s
    tmp = decimal.split('.')

    hours = adjust_hour_to_two_digits(tmp.first)
    minutes = adjust_minute_to_two_digits(tmp.last)
    "#{hours}:#{minutes}"
  end

  def self.get_total_seconds_from_time(time)
    time_splited = time.split(':')
    h = time_splited.first
    m = time_splited.last
    seconds = seconds_in("#{h} hours") + seconds_in("#{m} minutes")
    seconds.to_i
  end

  def self.convert_seconds_to_hour_format(seconds)
    [seconds / 3600, seconds / 60 % 60].map { |t| t.to_s.rjust(2, '0') }.join(':')
  end

  def self.adjust_hour_to_two_digits(hour)
    hour.rjust(2, '0')
  end

  def self.adjust_minute_to_two_digits(minute)
    minute.ljust(2, '0')
  end

  def self.seconds_in(time)
    now = Time.now
    Chronic.parse("#{time} from now", now: now) - now
  end
end
