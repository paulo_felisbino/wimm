# Why? #

## Motivation and context ##

This project intend to make the lives of our clients easier by doing the following actions on their own:

* Check how many hours they have hired
* Check how many hours are remaining
* Export .xls reports filtered by period and contracts hired.

------------------

# How? #

## Tech & versions ##

* Rails: 5.1.4
* Ruby: 2.3.3p222
* Database: MySQL
* Docker and docker-compose

## Environments ##

* Development

## Setting things up on development ##

* On root project, run:
> docker-compose run --service-ports web bash




* Inside the docker container, run:
> bundle install --path vendor/bundle
>
> rails db:create
>
> rails db:migrate
>
> rails db:seed
>
> rails s -b 0.0.0.0

* Open a new tab and run the following command to enter the cron container:
> docker exec -ti wimm_cron_1 bash

* Inside the docker container, run:
> bundle install
>
> RAILS_ENV=development whenever --update-crontab

* To check if crontab has been correctly updated, run:
> crontab -l

## Jobs ##

* __UpdateTasksJob__: job that reads user google drive spreadsheet and create tasks and contracts. Runned by a cron every day at 23:00hs local time.

------------------

# Important recommendations #

## Contribution guidelines ##

* __Do not push directly to branch master__. Please, create another branch and make use of [pull requests](https://bitbucket.org/paulo_felisbino/wimm/pull-requests).

* __Before pushing__ your code, please __run these commands__:
    * Linter for ruby
    > rubocop
    * Linter for javascript
    > jshint
    * Tests
    > rspec
    * Code quality check 1 (https://robots.thoughtbot.com/sandi-metz-rules-for-developers)
    > sandi_meter -dq
    * Code quality check 2
    > rails_best_practices .

* When __testing__, please refer to /coverage/index.html to ensure __at least 80%__ of code coverage (ideally, 100%)  

* When __commiting__, please refer to __[CONTRIBUTING.md](https://bitbucket.org/paulo_felisbino/wimm/src/020ff059d0efda95bf043eca3d843f120f0a77f0/CONTRIBUTING.md?at=master&fileviewer=file-view-default)__. It contains a __default format for a commit__.  

* When __creating a branch__, try following the following __format__:
    * For new features: __f-newFeature.user__ (ex: f-createReadme.paulofelisbino)
    * For a bug fix: __b-bugToFix.user__ (ex: b-fixPlansList.paulofelisbino)
    * For general stuff: __g-stuff.user__ (ex: g-lint.paulofelisbino)

## Who do I talk to? ##

Currently, the following people are working on this project:

* Paulo
